prometheus-redis-exporter (1.45.0-1) unstable; urgency=medium

  * New upstream release.
  * Switch to Standards-Version 4.6.1 (no changes needed).
  * Remove --help output from default file.
  * Update copyright years.

 -- Guillem Jover <gjover@sipwise.com>  Tue, 08 Nov 2022 19:31:20 +0100

prometheus-redis-exporter (1.37.0-2) unstable; urgency=medium

  * Set build variables correctly for --version and metrics.
    Reported by Michael Prokop <mprokop@sipwise.com>.

 -- Guillem Jover <gjover@sipwise.com>  Wed, 23 Mar 2022 09:48:44 +0100

prometheus-redis-exporter (1.37.0-1) unstable; urgency=medium

  * New upstream release.

 -- Guillem Jover <gjover@sipwise.com>  Tue, 22 Mar 2022 17:22:18 +0100

prometheus-redis-exporter (1.36.0-1) unstable; urgency=medium

  * New upstream release.

 -- Guillem Jover <gjover@sipwise.com>  Sat, 12 Mar 2022 01:26:49 +0100

prometheus-redis-exporter (1.33.0-1) unstable; urgency=medium

  * New upstream release.
  * Update man page and default file for new upstream release.

 -- Guillem Jover <gjover@sipwise.com>  Wed, 22 Dec 2021 20:20:53 +0100

prometheus-redis-exporter (1.32.0-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/watch file.
  * Move instead of copy when renaming the daemon in debian/rules.

 -- Guillem Jover <gjover@sipwise.com>  Mon, 13 Dec 2021 23:00:40 +0100

prometheus-redis-exporter (1.31.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Use dh-sequence-golang instead of dh-golang and --with=golang.
  * Switch to Standards-Version 4.6.0 (no changes needed).
  * Use @PACKAGE@ in debian/watch file instead of literal name.
  * Update gitlab-ci.yml from its new refactored template.
  * Update gbp.conf following latest Go Team workflow.
  * Update dependencies for new upstream release.
  * Update man page and default file for new upstream release.
  * Add myself to Uploaders.

 -- Guillem Jover <gjover@sipwise.com>  Mon, 29 Nov 2021 20:33:53 +0100

prometheus-redis-exporter (1.16.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Remove unnecessary DH_GOPKG variable from debian/rules
  * Update gitlab-ci.yml from latest upstream version

 -- Guillem Jover <gjover@sipwise.com>  Mon, 01 Feb 2021 18:46:04 +0100

prometheus-redis-exporter (1.15.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Indent debian/watch file
  * Update Maintainer and version.BuildUser with new team address
  * Add debian/upstream/metadata file

 -- Guillem Jover <gjover@sipwise.com>  Tue, 26 Jan 2021 17:30:07 +0100

prometheus-redis-exporter (1.13.1-2) unstable; urgency=medium

  * Team upload.
  * Change systemd service Restart directive from always to on-failure
  * Use '' instead of `' in text
  * Remove error suppression from postinst
  * Run adduser unconditionally
  * Remove $syslog dependency from init script
  * Add man:prometheus-redis-exporter(1) to systemd unit Documentation field
  * Switch from /var/run to /run
  * Rewrite init script using start-stop-daemon
  * Do not change pathname metadata if there are dpkg statoverrides in place
  * Switch to debhelper-compat 13, no changes needed
  * Update copyright years
  * Add missing dependency on adduser
  * Add support for reload action to sysvinit init script

 -- Guillem Jover <gjover@sipwise.com>  Sat, 23 Jan 2021 03:20:15 +0100

prometheus-redis-exporter (1.13.1-1) unstable; urgency=medium

  * Initial release, based on packaging work by Guillem Jover.
    (Closes: #954286)

 -- Michael Prokop <mika@debian.org>  Wed, 02 Dec 2020 23:21:19 +0100
